<?php
class Form 
{
	private $user_name;
	private $user_phone;
	private $user_address;
	private $user_address_2;
	private $honeypot;

	function __construct($user_data) {
		$this->user_name = $user_data['user_name'];
		$this->user_phone = $user_data['user_phone'];
		$this->user_address = $user_data['user_address'];
		$this->user_address_2 = $user_data['user_address_2'];
		$this->honeypot = $user_data['email']; // spam prevention input
	}

	public function validateUserInput() {
		if ($this->isUserNameValid($this->user_name) &&
			$this->isUserPhoneValid($this->user_phone) &&
			$this->isUserAddressValid($this->user_address) &&
			$this->isUserAddressValid($this->user_address_2) &&
			!$this->isUserBot($this->honeypot)) {
			return true;
		}
		return false;
	}

	public function writeUserDataToFile() {
		$user_name = $this->sanitizeInput($this->user_name);
		$user_phone = $this->sanitizeInput($this->user_phone);
		$user_address = $this->sanitizeInput($this->user_address);
		$user_address_2 = $this->sanitizeInput($this->user_address_2);
		$user_ip = $this->getUserIP();
		$data = "$user_name // $user_phone // $user_address // $user_address_2 // $user_ip\n";

		file_put_contents('logform.txt', $data, FILE_APPEND);
	}

	private function getUserIP() {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;			
	}

	private function isUserNameValid($user_name) {
		if (preg_match('/[^а-яА-ЯЁёa-zA-Z-]/u', $user_name) ||
			strlen($user_name) > 25) {
			return false;
		}
		return true;
	}

	private function isUserPhoneValid($user_phone) {
		if (preg_match('/[^0-9()\s+-]/', $user_phone) ||
			strlen($user_phone) > 25 ||
			strlen($user_phone) == 0) {
			return false;
		}
		return true;
	}

	private function isUserAddressValid($address) {
		if (strlen($address) > 0) return true;
		return false;
	}

	private function isUserBot($honeypot) {
		if (strlen($honeypot) > 0) return true;
		return false;
	}

	private function sanitizeInput($variable) {
		return filter_var($variable, FILTER_SANITIZE_STRING);
	}
}