<?php
if (!isset($_POST)) die();

require_once('Form.php');

$form = new Form($_POST);

if ($form->validateUserInput()) {
	$form->writeUserDataToFile();
	http_response_code(200);
}	else {
	http_response_code(400);
}		