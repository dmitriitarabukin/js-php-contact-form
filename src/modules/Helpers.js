export function debouncify(delay, fn) {
	let timer_id;
	return (...args) => {
		if (timer_id) clearTimeout(timer_id);
		return new Promise(resolve => {
			timer_id = setTimeout(() => {
				resolve(fn(...args));
			}, delay);
		});
	};
}
