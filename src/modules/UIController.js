import { FormErrors } from "./FormErrors";
import { FormAddressSuggestions } from "./FormAddressSuggestions";
import { FormModal } from "./FormModal";
import { search_control } from "./YandexMap";

function init() {
	Elements.form = document.querySelector(Selectors.form);
	Elements.user_name = document.querySelector(Selectors.user_name);
	Elements.user_phone = document.querySelector(Selectors.user_phone);
	Elements.address_1 = document.querySelector(Selectors.address_1);
	Elements.errors = FormErrors.init(Selectors.errors);
	Elements.address_suggestions = FormAddressSuggestions.init(
		Selectors.address_suggestions,
		Selectors.close_suggestions
	);
	Elements.modal = FormModal.init(
		Selectors.modal,
		Selectors.modal_message,
		Selectors.close_modal
	);
	Elements.address_2 = document.querySelector(Selectors.address_2);
	Elements.submit = document.querySelector(Selectors.submit);
}

let Elements = {
	form: null,
	user_name: null,
	user_phone: null,
	errors: null,
	address_1: null,
	address_suggestions: null,
	address_2: null,
	submit: null,
	modal: null
};

let Selectors = {
	form: '[rel="js-form"]',
	user_name: '[rel="js-user-name"]',
	user_phone: '[rel="js-user-phone"]',
	errors: '[rel="js-form-errors"]',
	address_1: '[rel="js-address"]',
	address_suggestions: '[rel="js-address-suggestions"]',
	close_suggestions: '[rel="js-close-suggestions"]',
	address_2: '[rel="js-address-2"]',
	submit: '[rel="js-form-submit"]',
	modal: '[rel="js-form-modal"]',
	modal_message: '[rel="js-modal-message"]',
	close_modal: '[rel="js-close-modal"]'
};

function toggleSubmitButtonText() {
	let submit_text = Elements.submit.value;

	if (Elements.submit.value === "Отправить") {
		Elements.submit.value = "Идет отправка";
	} else {
		Elements.submit.value = "Отправить";
	}
}

function clearForm() {
	Elements.user_name.value = "";
	Elements.user_phone.value = "";
	Elements.address_1.value = "";
	Elements.address_2.value = "";
	search_control.clear();
}

export {
	init as initUI,
	Selectors,
	Elements,
	toggleSubmitButtonText,
	clearForm
};
