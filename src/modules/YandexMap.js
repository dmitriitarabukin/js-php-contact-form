import { Elements } from "./UIController";

let yandex_map, search_control;

function init() {
	yandex_map = new ymaps.Map("map", {
		center: [59.93, 30.31],
		zoom: 9,
		controls: []
	});
	search_control = new ymaps.control.SearchControl();

	yandex_map.controls.add(search_control);

	search_control.events.add("resultselect", event => {
		let index = event.get("index");
		search_control.getResult(index).then(result => {
			Elements.address_2.value = search_control.getRequestString();
		});
	});
}

export { init, search_control };
