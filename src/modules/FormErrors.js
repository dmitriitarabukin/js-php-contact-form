import { Selectors } from "./UIController";

export let FormErrors = {
	container: null,

	init: function init(container) {
		this.container = document.querySelector(container);
		return this;
	},

	show: function show(errors) {
		this.clear();
		errors.forEach(error => {
			this.container.appendChild(this._create(error));
		});
	},

	_create: function _create(error) {
		let error_elem = document.createElement("span");
		error_elem.innerText = error;
		return error_elem;
	},

	clear: function clear() {
		this.container.innerHTML = "";
	}
};
