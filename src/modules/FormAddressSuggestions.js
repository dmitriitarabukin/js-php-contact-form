import { Elements } from "./UIController";

export let FormAddressSuggestions = {
	container: null,
	close_button: null,

	init: function init(container, close_button) {
		this.container = document.querySelector(container);
		this.close_button = document.querySelector(close_button);
		this.container.addEventListener(
			"click",
			this._setAddressSuggestionHandler.bind(this)
		);
		this.close_button.addEventListener(
			"click",
			this._closeButtonHandler.bind(this)
		);
		return this;
	},

	show: function show(suggestions) {
		if (!suggestions.length) return;
		this._clear();
		this._showCloseButton();
		this.container.classList.add("visible");
		suggestions.forEach(suggestion => {
			this.container.appendChild(this._create(suggestion));
		});
	},

	hide: function hide() {
		this.container.classList.remove("visible");
		this._hideCloseButton();
		this._clear();
	},

	_create: function _create(suggestion) {
		let suggestion_elem = document.createElement("span");
		suggestion_elem.innerText = suggestion;
		suggestion_elem.setAttribute("rel", "js-address-suggestion");
		suggestion_elem.setAttribute("title", suggestion);
		return suggestion_elem;
	},

	_clear: function _clear() {
		this.container.innerHTML = "";
	},

	_showCloseButton: function _showCloseButton() {
		this.close_button.classList.add("visible");
	},

	_hideCloseButton: function _hideCloseButton() {
		this.close_button.classList.remove("visible");
	},

	_setAddressSuggestionHandler: function _setAddressSuggestionHandler(event) {
		if (event.target.getAttribute("rel") === "js-address-suggestion") {
			Elements.address_1.value = event.target.innerText;
			this.hide();
		}
	},

	_closeButtonHandler: function _closeButtonHandler(event) {
		event.preventDefault();
		this.hide();
	}
};
