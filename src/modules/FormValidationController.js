import { Elements } from "./UIController";

const ERROR_MESSAGES = {
	user_name_length: "Имя должно содержать не более 25 символов.",
	user_name_characters: "Имя должно состоять только из букв и тире.",
	user_phone_required: "Телефон обязателен для заполнения.",
	user_phone_length: "Телефон должен содержать не более 25 символов.",
	user_phone_characters: "В номере телефона введены недопустимые символы.",
	user_address_required: "Адрес обязателен для заполнения.",
	user_address_2_required: "Адрес 2 обязателен для заполнения.",
	user_is_bot: "Вы бот."
};

function validateForm(form_data) {
	let user_name = form_data.get("user_name"),
		user_phone = window.intlTelInputGlobals
			.getInstance(Elements.user_phone)
			.getNumber(),
		user_address = form_data.get("user_address"),
		user_address_2 = form_data.get("user_address_2"),
		honeypot = form_data.get("email"); // spam prevention input

	let error_messages = [
		..._validateUserName(user_name),
		..._validateUserPhone(user_phone),
		..._validateUserAddress(user_address),
		..._validateUserAddress2(user_address_2),
		..._validateIfUserIsBot(honeypot)
	];
	
	return error_messages;
}

function _validateUserName(name) {
	let user_name_error_messages = [];
	if (name.length > 25)
		user_name_error_messages.push(ERROR_MESSAGES.user_name_length);
	if (/[^а-яА-ЯЁёa-zA-Z-]/.test(name))
		user_name_error_messages.push(ERROR_MESSAGES.user_name_characters);
	return user_name_error_messages;
}

function _validateUserPhone(phone) {
	let user_phone_error_messages = [];
	if (!phone.length)
		user_phone_error_messages.push(ERROR_MESSAGES.user_phone_required);
	if (phone.length > 25)
		user_phone_error_messages.push(ERROR_MESSAGES.user_phone_length);
	if (/[^0-9()\s+-]/.test(phone))
		user_phone_error_messages.push(ERROR_MESSAGES.user_phone_characters);
	return user_phone_error_messages;
}

function _validateUserAddress(address) {
	let user_address_error_messages = [];
	if (!address.length)
		user_address_error_messages.push(ERROR_MESSAGES.user_address_required);
	return user_address_error_messages;
}

function _validateUserAddress2(address) {
	let user_address_2_error_messages = [];
	if (!address.length)
		user_address_2_error_messages.push(ERROR_MESSAGES.user_address_2_required);
	return user_address_2_error_messages;
}

function _validateIfUserIsBot(text) {
	let user_is_bot_error_messages = [];
	if (text.length) user_is_bot_error_messages.push(ERROR_MESSAGES.user_is_bot);
	return user_is_bot_error_messages;
}

export { validateForm };
