export let FormModal = {
	container: null,
	message: null,
	close_button: null,

	init: function init(container, message, close_button) {
		this.container = document.querySelector(container);
		this.message = document.querySelector(message);
		this.close_button = document.querySelector(close_button);
		window.addEventListener("click", this.close.bind(this));
		return this;
	},

	show: function show(message) {
		this.message.innerText = message;
		this.container.classList.add("visible");
	},

	close: function close(event) {
		let event_target = event.target;
		if (event_target === this.container || event_target === this.close_button) {
			this.container.classList.remove("visible");
			this._clear();
		}
	},

	_clear: function _clear() {
		this.message.innerText = "";
	}
};
