export function fetchSuggestions(query) {
	return ymaps.suggest(query).then(items => {
		return items;
	});
}

export function fetchCountryCodeByIP() {
	return fetch("https://api.ipgeolocation.io/ipgeo?apiKey=17640dfe368c4c9788c05cf53b880b2a").then(response => {
		return response.json();
	});
}

export function submitForm(form_data) {
	return fetch("formHandler.php", {
		method: "POST",
		body: form_data
	}).then(response => {
		if (response.ok) {
			return response.text();
		}
		throw new Error("Произошла ошибка");
	});
}
