import {
	initUI,
	Selectors,
	Elements,
	toggleSubmitButtonText,
	clearForm
} from "./UIController";
import { validateForm } from "./FormValidationController";
import "intl-tel-input/build/js/utils";
import intlTelInput from "intl-tel-input";
import {
	fetchSuggestions,
	fetchCountryCodeByIP,
	submitForm
} from "./FetchController";
import { init as initYandexMap } from "./YandexMap";
import { debouncify } from "./Helpers";

let fetchSuggestionsDebouncified = debouncify(400, fetchSuggestions);

function init() {
	initUI();
	Elements.form.addEventListener("submit", formSubmitHandler);
	Elements.address_1.addEventListener("input", addressChangeHandler);
	intlTelInput(Elements.user_phone, {
		initialCountry: "auto",
		geoIpLookup: (success, failure) => {
			fetchCountryCodeByIP().then(result => {
				success(result.country_code2);
			});
		}
	});
	ymaps.ready(initYandexMap);
}

function formSubmitHandler(event) {
	event.preventDefault();

	let form_data = new FormData(event.target),
		errors = validateForm(form_data);

	if (errors.length) {
		Elements.errors.show(errors);
		Elements.modal.show("Произошла ошибка");
		return;
	} else {
		Elements.errors.clear();
	}

	toggleSubmitButtonText();

	submitForm(form_data)
		.then(result => {
			Elements.modal.show("Успешно отправлено");
			toggleSubmitButtonText();
			clearForm();
		})
		.catch(error => {
			Elements.modal.show("Произошла ошибка");
			toggleSubmitButtonText();
			console.error(error);
		});
}

function addressChangeHandler(event) {
	event.stopImmediatePropagation();
	event.preventDefault();
	fetchSuggestionsDebouncified(event.target.value)
		.then(result => {
			Elements.address_suggestions.show(
				result.map(suggestion => suggestion.value)
			);
		})
		.catch(error => {
			console.error(error);
		});
}

export { init as initApp };
