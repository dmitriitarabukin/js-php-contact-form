import "skeleton-css/css/normalize.css";
import "skeleton-css/css/skeleton.css";
import "intl-tel-input/build/css/intlTelInput.css";
import "./sass/styles.sass";
import { initApp } from "./modules/App";

window.addEventListener("DOMContentLoaded", function bootstrapApp() {
	initApp();
});
